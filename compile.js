const protobuf = require('protocol-buffers')
const path = require('path')
const fs = require('fs')

let pending = 0
getSchemas(path.join('encoders'))

function getSchemas(basePath) {
  fs.readdir(path.join(basePath), (err, files) => {
    pending += files.length
    next(0)

    function next (n) {
      var file = files[n]
      if (!file) return done()

      process.nextTick(next, n + 1)

      var currentPath = path.join(basePath, file)
      fs.lstat(currentPath, (err, stat) => {
        if (err) return done(err)
        if (!stat.isFile()) {
          getSchemas(currentPath)
          return done()
        }
        if (file.match(/.proto/)) {
          fs.readFile(currentPath, (err, buf) => {
            if (err) return done(err)
            fs.writeFile(
              path.join(basePath, 'index.js'),
              protobuf.toJS(buf),
              done
            )
          })
        }
      })
    }
  })
}

function done (err) {
  if (err) {
    pending = Infinity
    return console.error(err)
  }
  if (!--pending) console.log("compiled schemas")
}
