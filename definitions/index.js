const msgIdRegex = '^[0-9a-fA-F]{0,64}@\d+$'
const feedIdRegex = '^[0-9a-fA-F]{0,64}$'
const peerIdRegex = '^[0-9a-fA-F]{0,64}$'
const inviteCodeRegex = '^[0-9a-fA-F]{0,272}'
const signatureRegex = '^[0-9a-fA-F]{0,128}$'

module.exports = {
  messageId: {
    type: 'string',
    pattern: msgIdRegex
  },
  feedId: {
    type: 'string',
    pattern: feedIdRegex
  },
  peerId: {
    type: 'string',
    pattern: peerIdRegex
  },
  signature: {
    type: 'string',
    pattern: signatureRegex
  },
  inviteCode: {
    type: 'string',
    pattern: inviteCodeRegex
  }
}
