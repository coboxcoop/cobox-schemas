const { describe } = require('tape-plus')
const crypto = require('crypto')

const { encodings, validators } = require('../../')
const Announce = encodings.command.announce
const isAnnounce = validators.command.announce
const author = crypto.randomBytes(32).toString('hex')

describe('Announce: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "command/announce"
    }
    assert.ok(Announce.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author
    }
    assert.throws(() => Announce.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "command/announce"
    }
    assert.throws(() => Announce.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "command/announce"
    }
    assert.throws(() => Announce.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('Announce: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "command/announce"
    }
    assert.ok(isAnnounce(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author
    }
    assert.notOk(isAnnounce(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "command/announce"
    }
    assert.notOk(isAnnounce(msg), 'invalid without timestamp')
    next()
  })
})
