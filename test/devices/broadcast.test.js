const { describe } = require('tape-plus')
const Factory = require('../../')
const crypto = require('crypto')

const { encodings, validators } = Factory
const Broadcast = encodings.devices.broadcast
const isBroadcast = validators.devices.broadcast
const author = crypto.randomBytes(32).toString('hex')

describe('DevicesBroadcast: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      type: "devices/broadcast",
      timestamp: 1580584726389,
      author,
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.ok(Broadcast.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "devices/broadcast",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "devices/broadcast",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/broadcast"
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('DevicesBroadcast: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/broadcast",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.ok(isBroadcast(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.notOk(isBroadcast(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "devices/broadcast",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.notOk(isBroadcast(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/broadcast"
    }
    assert.notOk(isBroadcast(msg), 'invalid without timestamp')
    next()
  })
})

