const { describe } = require('tape-plus')
const Factory = require('../../')
const crypto = require('crypto')

const { encodings, validators } = Factory
const Confirm = encodings.devices.confirm
const isConfirm = validators.devices.confirm
const author = crypto.randomBytes(32).toString('hex')

describe('Confirm: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      type: "devices/confirm",
      timestamp: 1580584726389,
      author,
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.ok(Confirm.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.throws(() => Confirm.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "devices/confirm",
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.throws(() => Confirm.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "devices/confirm",
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.throws(() => Confirm.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/confirm"
    }
    assert.throws(() => Confirm.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('Confirm: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/confirm",
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.ok(isConfirm(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.notOk(isConfirm(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "devices/confirm",
      content: {
        signature: crypto.randomBytes(64).toString('hex')
      }
    }
    assert.notOk(isConfirm(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "devices/confirm"
    }
    assert.notOk(isConfirm(msg), 'invalid without timestamp')
    next()
  })
})

