const { describe } = require('tape-plus')
const Factory = require('../../')
const crypto = require('crypto')

const { encodings, validators } = Factory
const About = encodings.space.about
const isAbout = validators.space.about
const author = crypto.randomBytes(32).toString('hex')

describe('SpaceAbout: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "space/about",
      content: {
        name: "magma"
      }
    }
    assert.ok(About.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "space/about",
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "space/about",
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author
    }
    assert.throws(() => About.encode(msg), 'Error: content is required', 'throws when missing content')
    next()
  })
})

describe('SpaceAbout: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "space/about",
      content: {
        name: "magma"
      }
    }
    assert.ok(isAbout(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      content: {
        name: "magma"
      }
    }
    assert.notOk(isAbout(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "space/about",
      content: {
        name: "magma"
      }
    }
    assert.notOk(isAbout(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author
    }
    assert.notOk(isAbout(msg), 'invalid without content')
    next()
  })
})

