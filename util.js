const _ = require('lodash')

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}
function camelCase (str) {
  return str.replace(/[/_.-](\w|$)/g, (_,x) => x.toUpperCase())
}

function capitalise (str) {
  return str[0] ? `${str[0].toUpperCase()}${str.substring(1)}` : '';
}

function removeEmpty(obj) {
  return function remove (current) {
    _.forOwn(current, function (value, key) {
      if (_.isUndefined(value) || _.isNull(value) || _.isNaN(value) ||
        (_.isString(value) && _.isEmpty(value)) ||
        (_.isObject(value) && _.isEmpty(remove(value)))) {
        delete current[key]
      }
    });
    if (_.isArray(current)) _.pull(current, undefined)

    return current

  }(_.cloneDeep(obj))
}

function formatError (err) {
  var msg = err.message
  var field = 'data.'.concat(msg.slice(0, msg.length - 11).trim())
  var message = msg.slice(msg.length - 11, msg.length).trim()
  return [{ field, message }]
}

module.exports = {
  hex,
  capitalise,
  camelCase,
  removeEmpty,
  formatError
}
