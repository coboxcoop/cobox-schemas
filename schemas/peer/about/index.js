const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'author', 'timestamp', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^peer/about'
    },
    author: { $ref: '#/definitions/peerId' },
    timestamp: { type: 'integer' },
    content: {
      type: 'object',
      oneof: [{
        properties: {
          name: { type: 'string' }
        }
      }]
    }
  },
  definitions
}
