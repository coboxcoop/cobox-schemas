const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'timestamp', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^peer/connection'
    },
    timestamp: { type: 'integer' },
    content: {
      type: 'object',
      properties: {
        connected: { type: 'boolean' },
        peer: { $ref: '#/definitions/peerId' }
      }
    }
  },
  definitions
}
