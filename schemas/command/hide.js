const definitions = require('../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['author', 'timestamp', 'type'],
  properties: {
    type: {
      type: 'string',
      pattern: '^command/hide'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' }
  },
  definitions
}
