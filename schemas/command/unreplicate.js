const definitions = require('../../definitions')

// orders cobox-hub to stop replicating given address

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'timestamp', 'author', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^command/unreplicate'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
    content: {
      type :'object',
      required: ['name', 'address'],
      properties: {
        name: { type: 'string' },
        address: { $ref: '#/definitions/feedId' },
      }
    }
  },
  definitions
}
