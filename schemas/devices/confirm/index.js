const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['author', 'timestamp', 'type', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^devices/confirm'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
    content: {
      type: 'object',
      required: ['signature'],
      properties: {
        signature: { $ref: '#/definitions/signature' }
      }
    }
  },
  definitions
}
