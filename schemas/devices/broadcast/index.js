const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['author', 'timestamp', 'type', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^devices/broadcast'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
    content: {
      type: 'object',
      required: ['address', 'encryptionKey'],
      properties: {
        address: { $ref: '#/definitions/feedId' },
        encryptionKey: { $ref: '#/definitions/feedId' }
      }
    }
  },
  definitions
}
