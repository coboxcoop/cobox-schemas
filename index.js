const Validator = require('is-my-json-valid')
const path = require('path')
const fs = require('fs')
const get = require('lodash.get')

const { hex, removeEmpty, formatError, camelCase, capitalise } = require('./util')
const schemas = require('./schemas')

const basePath = path.join(__dirname)

function buildValidators (key, schemaSpace) {
  if (!schemaSpace.$schema) {
    return Object.keys(schemaSpace).reduce((acc, key) => {
      acc[key] = buildValidators(key, schemaSpace[key])
      return acc
    }, {})
  } else {
    return validator(schemaSpace)
  }

  function validator (schema) {
    const validate = Validator(schema)

    return function isValid (msg, opts = {}) {
      const result = validate(msg)
      isValid.errors = validate.errors
      if (opts.attachErrors && validate.errors) msg.errors = isValid.errors
      return result
    }
  }
}

function buildEncodings (currentPath) {
  return fs
    .readdirSync(path.join(basePath, currentPath || '/'))
    .reduce((acc, filename) => {
      var stat = fs.lstatSync(path.join(basePath, currentPath, filename))
      if (stat.isFile()) {
        if (filename.match(/.js$/)) {
          let schema = require(path.join(basePath, currentPath)).Schema
          Object.assign(acc, schema)
        }
      } else {
        acc[filename] = buildEncodings(path.join(currentPath, filename))
      }
      return acc
    }, {})
}

const encodings = module.exports.encodings = buildEncodings('encoders')
const validators = module.exports.validators = buildValidators(null, schemas)
const definitions = module.exports.definitions = require('./definitions')
