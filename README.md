# cobox-schemas

This module exports a set of object factories for use with a hypercore. Factories build payloads for a cobox-log using JSON schemas for validation and Protocol Buffers for encoding.

This module also exports all currently used message schemas in CoBox as raw validators and encoders.

In order to use the protocol-buffers message encoders, you'll need to pre-compile them.

```
npm run compile
```

```js
const Factory = require('../')
const author = 'a94fd23d17443e336b798642db689492344c9a35dc37d93e529acd4a432c64c3'
const { PeerAbout, SpaceAbout } = Factory(author)

// Lets build a new peer/about message with a name...
PeerAbout({ name: 'magpie' }, (err, payload) => {
  // The returned protocol buffer is an encoded message which when decoded takes the following format
  // {
  //   timestamp: 1580748966968,
  //   author: 'a94fd23d17443e336b798642db689492344c9a35dc37d93e529acd4a432c64c3',
  //   content: {
  //     type: 'peer/about',
  //     name: 'magpie'
  //   }
  // }

  // Now lets do the same for the name of a space...
  SpaceAbout({ name: 'Magma Collective' }, (err, payload) => {
  // {
  //   timestamp: 1580748966968,
  //   author: 'a94fd23d17443e336b798642db689492344c9a35dc37d93e529acd4a432c64c3',
  //   content: {
  //     type: 'space/about',
  //     name: 'Magma Collective'
  //   }
  // }
  })
})
```
